using System.Collections.Generic;
using System.Drawing;

namespace DroneMapper
{

    class Node
    {
        public string Name { get; }
        public Point Location { get; } 
        public Node(Point location, string name)
        {
            Location = location;
            Name = name;
        }
    }
}