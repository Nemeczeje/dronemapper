﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DroneMapper
{
    public class Constants
    {

        public static readonly GPSCoordinates StartPoint = new GPSCoordinates()
        {
            latitude = 50.088244,
            longitude = 20.203471,
            utm = new Utm()
            {
                easting = 443020,
                northing = 5548846,
                zone_number = 34,
                zone_letter = "U"
            }
        };

        public static readonly Dictionary<string, GPSCoordinates> Points = new Dictionary<string, GPSCoordinates>()
        {
            {
                "LAK01",
                new GPSCoordinates()
                {
                    latitude = 50.093091,
                    longitude = 20.191591
                }
            },
            {
                "LAK02",
                new GPSCoordinates()
                {
                    latitude = 50.093167,
                    longitude = 20.20002
                }
            },
            {
                "LAK03",
                new GPSCoordinates()
                {
                    latitude = 50.093249 ,
                    longitude = 20.206618
                }
            },
               {
                "LAK04",
                new GPSCoordinates()
                {
                    latitude = 50.09133 ,
                    longitude =  20.206174
                }
            },
               {
                "LAK05",
                new GPSCoordinates()
                {
                    latitude = 50.090358 ,
                    longitude =  20.208735
                }
            },
               {
                "LAK06",
                new GPSCoordinates()
                {
                    latitude = 50.088489 ,
                    longitude =  20.208989
                }
            },
                 {
                "LAK07",
                new GPSCoordinates()
                {
                    latitude = 50.08767,
                    longitude =  20.210022
                }
            },
                   {
                "LAK08",
                new GPSCoordinates()
                {
                    latitude = 50.088138,
                    longitude =  20.200286
                }
            },
                    {
                "LAK09",
                new GPSCoordinates()
                {
                    latitude = 50.088435,
                    longitude =   20.192006
                }
            },

                    {
                "LAK10",
                new GPSCoordinates()
                {
                    latitude =  50.090611  ,
                    longitude =   20.191955
                }
            },

                    {
                "LAK11",
                new GPSCoordinates()
                {
                    latitude =  50.091635  ,
                    longitude =   20.191672
                }
            },



        };
    }
}
