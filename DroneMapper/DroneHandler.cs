﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DroneMapper
{
    class DroneHandler
    {
        public List<Drone> Drones { get; private set; }


        LocationReader reader = new LocationReader();

        public DroneHandler()
        {
            Drones = new List<Drone>()
            {
//                new Drone()
//                {
//                    Name ="Czerwony",
//                    CoordinatesUrl = @"http://192.168.88.252:8001/gps"
//                },
//                new Drone()
//                {
//                    Name ="Hexa",
//                    CoordinatesUrl = @"http://192.168.88.252:8002/gps"
//                },
                new Drone()
                {
                    Name ="Hexa",
                    CoordinatesUrl = @"http://158.69.48.247:8001/read-gps"
                }
            };
        }


        public void UpdatePositions()
        {
            foreach (var drone in Drones)
            {
                drone.GpsCoordinates = reader.ReadGpsCoordinates(drone.CoordinatesUrl);
            }
        }

    }
}
