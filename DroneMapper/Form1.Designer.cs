﻿namespace DroneMapper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.mapPanel = new System.Windows.Forms.Panel();
            this.rangePanel = new System.Windows.Forms.Panel();
            this.buttonsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.saveMap = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.foundPointsListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PointInfo = new System.Windows.Forms.Label();
            this.SavePerson = new System.Windows.Forms.Button();
            this.SaveObject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mapPanel
            // 
            this.mapPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mapPanel.BackgroundImage")));
            this.mapPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapPanel.Location = new System.Drawing.Point(12, 12);
            this.mapPanel.Name = "mapPanel";
            this.mapPanel.Size = new System.Drawing.Size(950, 550);
            this.mapPanel.TabIndex = 0;
            this.mapPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // rangePanel
            // 
            this.rangePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rangePanel.Location = new System.Drawing.Point(980, 12);
            this.rangePanel.Name = "rangePanel";
            this.rangePanel.Size = new System.Drawing.Size(317, 183);
            this.rangePanel.TabIndex = 1;
            this.rangePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.ColumnCount = 1;
            this.buttonsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonsPanel.Location = new System.Drawing.Point(980, 201);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.RowCount = 6;
            this.buttonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.buttonsPanel.Size = new System.Drawing.Size(144, 349);
            this.buttonsPanel.TabIndex = 2;
            // 
            // saveMap
            // 
            this.saveMap.Location = new System.Drawing.Point(1130, 201);
            this.saveMap.Name = "saveMap";
            this.saveMap.Size = new System.Drawing.Size(158, 60);
            this.saveMap.TabIndex = 3;
            this.saveMap.Text = "Zapisz Mape";
            this.saveMap.UseVisualStyleBackColor = true;
            this.saveMap.Click += new System.EventHandler(this.saveMap_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1169, 527);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // foundPointsListBox
            // 
            this.foundPointsListBox.FormattingEnabled = true;
            this.foundPointsListBox.Location = new System.Drawing.Point(1130, 426);
            this.foundPointsListBox.Name = "foundPointsListBox";
            this.foundPointsListBox.Size = new System.Drawing.Size(158, 95);
            this.foundPointsListBox.TabIndex = 6;
            this.foundPointsListBox.SelectedIndexChanged += new System.EventHandler(this.foundPointsListBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1166, 410);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Saved points";
            // 
            // PointInfo
            // 
            this.PointInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PointInfo.Location = new System.Drawing.Point(1130, 264);
            this.PointInfo.Name = "PointInfo";
            this.PointInfo.Size = new System.Drawing.Size(158, 119);
            this.PointInfo.TabIndex = 8;
            this.PointInfo.UseMnemonic = false;
            // 
            // SavePerson
            // 
            this.SavePerson.Location = new System.Drawing.Point(1133, 386);
            this.SavePerson.Name = "SavePerson";
            this.SavePerson.Size = new System.Drawing.Size(75, 23);
            this.SavePerson.TabIndex = 9;
            this.SavePerson.Text = "Czlowiek";
            this.SavePerson.UseVisualStyleBackColor = true;
            this.SavePerson.Click += new System.EventHandler(this.SavePerson_Click);
            // 
            // SaveObject
            // 
            this.SaveObject.Location = new System.Drawing.Point(1213, 386);
            this.SaveObject.Name = "SaveObject";
            this.SaveObject.Size = new System.Drawing.Size(75, 23);
            this.SaveObject.TabIndex = 10;
            this.SaveObject.Text = "Obiekt";
            this.SaveObject.UseVisualStyleBackColor = true;
            this.SaveObject.Click += new System.EventHandler(this.SaveObject_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 562);
            this.Controls.Add(this.SaveObject);
            this.Controls.Add(this.SavePerson);
            this.Controls.Add(this.PointInfo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.foundPointsListBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.saveMap);
            this.Controls.Add(this.rangePanel);
            this.Controls.Add(this.buttonsPanel);
            this.Controls.Add(this.mapPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel mapPanel;
        private System.Windows.Forms.Panel rangePanel;
        private System.Windows.Forms.TableLayoutPanel buttonsPanel;
        private System.Windows.Forms.Button saveMap;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label PointInfo;
        private System.Windows.Forms.Button SavePerson;
        private System.Windows.Forms.Button SaveObject;
        private System.Windows.Forms.ListBox foundPointsListBox;
    }
}

