﻿using System;
using System.Drawing;

namespace DroneMapper
{
    class Drone
    {
        public GPSCoordinates GpsCoordinates { get; set; }
        public string CoordinatesUrl { get; set; }
        public Point Location => GpsCoordinates.AsLocationOnPicture;
        public string Name { get; set; }
        public int Range => GpsCoordinates.altitude>0?Convert.ToInt32(GpsCoordinates.altitude*3):1;
    }
}
