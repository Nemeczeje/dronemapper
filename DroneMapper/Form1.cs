﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Windows.Forms;

namespace DroneMapper
{
    public partial class Form1 : Form
    {

        int millisecondsTimeout = 100;

        DroneHandler droneHandler = new DroneHandler();
        SolidBrush droneFillBrush = new SolidBrush(Color.Yellow);
        SolidBrush droneRangeBrush = new SolidBrush(Color.FromArgb(50, Color.Yellow));
        SolidBrush droneIconBrush = new SolidBrush(Color.Black);
        int droneIconSize = 5;
        int rangePanelMultiplayer = 3;

        private List<Label> droneLabels = new List<Label>();
        private List<Button> droneButtons = new List<Button>();

        private Dictionary<string, Point> droneFindings = new Dictionary<string, Point>();

        
        class PointInListObject
        {
        
            public PointInListObject(GPSCoordinates gpsCoordinates)
            {
                GpsCoordinates = gpsCoordinates;
            }

            public GPSCoordinates GpsCoordinates { get; private set; }

            public override string ToString()
            {
                return $"{GpsCoordinates.latitude:0.000000}N {GpsCoordinates.longitude:0.000000}E";
            }
        }

        private GPSCoordinates selectedPosition;
        private GPSCoordinates personCoordinates;
        private GPSCoordinates objectCoordinates;

        private BindingList<PointInListObject> foundPoints = new BindingList<PointInListObject>();

        public Form1()
        {
            InitializeComponent();

            buttonsPanel.RowCount = 2*droneHandler.Drones.Count;
            InitializeDroneButtons();
            foundPointsListBox.DataSource = foundPoints;

            DateTime someStart = DateTime.Now.AddSeconds(1);
            TimeSpan someInterval = TimeSpan.FromMilliseconds(millisecondsTimeout*10);
            StartTimer(someStart, someInterval, DrawDrones);
        }

        private void InitializeDroneButtons()
        {
            for (int i = 0; i < droneHandler.Drones.Count; i++)
            {
                var drone = droneHandler.Drones[i];
                AddDroneButton(drone);
                AddDroneLabel();
            }
        }

        private void AddDroneLabel()
        {
            var label = new Label()
            {
                Font = new Font(FontFamily.GenericSansSerif, 11.0F, FontStyle.Bold),
                Size = new Size(200, 80)
            };
            droneLabels.Add(label);
            buttonsPanel.Controls.Add(label);
        }

        private void AddDroneButton(Drone drone)
        {
            var button = new Button()
            {
                Text = drone.Name,
                Size = new Size(200, 30)
            };
            button.Click += new EventHandler(OnDroneButtonClick);
            droneButtons.Add(button);
            buttonsPanel.Controls.Add(button);
        }

        private bool DrawDrones()
        {
            droneHandler.UpdatePositions();
            UpdateLabels();
            DrawSecondPanel();
            mapPanel.Invalidate();
            return true;
        }

        private void DrawMainPanel()
        {
            var graphicsMainPanel = mapPanel.CreateGraphics();

            foreach (var drone in droneHandler.Drones)
            {
                if (drone.GpsCoordinates != null)
                {
                    graphicsMainPanel.FillEllipse(droneRangeBrush, drone.Location.X - drone.Range/2,
                        drone.Location.Y - drone.Range/2, drone.Range, drone.Range);

                    graphicsMainPanel.FillEllipse(droneIconBrush, drone.Location.X, drone.Location.Y, droneIconSize,
                        droneIconSize);

                    graphicsMainPanel.DrawString(drone.Name, new Font("Tahoma", 15, FontStyle.Bold), Brushes.Black,
                        drone.Location.X, drone.Location.Y);
                }
            }

            drawSelectedPosition(graphicsMainPanel);

            DrawConstantPoints(graphicsMainPanel);

            DrawBestPaths(graphicsMainPanel);
        }

        private void DrawConstantPoints(Graphics graphicsMainPanel)
        {
            var linePen = new Pen(Color.Blue, 4);
            var pointColor = Brushes.Blue;
            var previous = Constants.Points.Last().Value.AsLocationOnPicture;
            foreach (var point in Constants.Points)
            {
                DrawPointWithSignature(graphicsMainPanel, point.Value.AsLocationOnPicture, point.Key, pointColor);
                graphicsMainPanel.DrawLine(linePen,point.Value.AsLocationOnPicture, previous);
                previous = point.Value.AsLocationOnPicture;

            }
        }

        private void drawSelectedPosition(Graphics graphicsMainPanel)
        {
            if (selectedPosition != null)
            {
                var position = selectedPosition.AsLocationOnPicture;
                var pSize = 20;
                graphicsMainPanel.FillEllipse(new SolidBrush(Color.Blue), position.X - pSize/2, position.Y - pSize/2,
                    pSize, pSize);
            }
        }

        private void DrawSecondPanel()
        {
            var graphicsSecondPanel = rangePanel.CreateGraphics();

            foreach (var drone in droneHandler.Drones)
            {
                if (drone.GpsCoordinates != null)
                {
                    graphicsSecondPanel.FillEllipse(droneFillBrush,
                        (drone.Location.X - drone.Range/2)/rangePanelMultiplayer,
                        (drone.Location.Y - drone.Range/2)/rangePanelMultiplayer, drone.Range/rangePanelMultiplayer,
                        drone.Range/rangePanelMultiplayer);
                }
            }
        }

        //The following is the actual guts.. and can be transplanted to an actual class.
        private delegate void voidFunc<P1, P2, P3>(P1 p1, P2 p2, P3 p3);

        public void StartTimer(DateTime startTime, TimeSpan interval, Func<bool> action)
        {
            voidFunc<DateTime, TimeSpan, Func<bool>> Timer = TimedThread;
            Timer.BeginInvoke(startTime, interval, action, null, null);
        }

        private void OnDroneButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button) sender;
            var drone = droneHandler.Drones.SingleOrDefault(d => d.Name == btn.Text);
            if (drone.GpsCoordinates != null)
            {
                foundPoints.Add(new PointInListObject(drone.GpsCoordinates));
                foundPointsListBox.SelectedItem = null;
                foundPointsListBox.SelectedItem = foundPointsListBox.Items[foundPointsListBox.Items.Count - 1];
            }
        }

        private void TimedThread(DateTime startTime, TimeSpan interval, Func<bool> action)
        {
            bool keepRunning = true;
            DateTime NextExecute = startTime;
            while (keepRunning) 
            {
                if (DateTime.Now > NextExecute)
                {
                    keepRunning = action.Invoke();
                    NextExecute = NextExecute.Add(interval);
                }
                //could parameterize resolution.
                Thread.Sleep(millisecondsTimeout);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            var graphicsMainPanel = mapPanel.CreateGraphics();
            DrawMainPanel();
        }





        double GetDistanceFromStartTo(GPSCoordinates end)
        {
            return DistanceBetweenCoordinates(Constants.StartPoint, end);
        }

        private static double DistanceBetweenCoordinates(GPSCoordinates start, GPSCoordinates end)
        {
            var lat1 = start.latitude;
            var lat2 = end.latitude;
            var lon1 = start.longitude;
            var lon2 = end.longitude;

            var R = 6378.137; // Radius of earth in KM
            var dLat = (lat2 - lat1)*Math.PI/180;
            var dLon = (lon2 - lon1)*Math.PI/180;
            var a = Math.Sin(dLat/2)*Math.Sin(dLat/2) +
                    Math.Cos(lat1*Math.PI/180)*Math.Cos(lat2*Math.PI/180)*Math.Sin(dLon/2)*Math.Sin(dLon/2);
            var c = 2*Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R*c;
            return d*1000; // meters
        }

        private void UpdateLabels()
        {
            for (int i = 0; i < droneHandler.Drones.Count; i++)
            {
                if (droneHandler.Drones[i].GpsCoordinates != null)
                    droneLabels[i].Text = $"Lat:  {droneHandler.Drones[i].GpsCoordinates.latitude:0.000000}N\n" +
                                          $"Lon: {droneHandler.Drones[i].GpsCoordinates.longitude:0.000000}E\n" +
                                          $"Alt:   {droneHandler.Drones[i].GpsCoordinates.altitude:0.00}m \n" +
                                          $"Dist: {GetDistanceFromStartTo(droneHandler.Drones[i].GpsCoordinates):0.00}m";
                else
                    droneLabels[i].Text = "No  gps signal";
            }
        }

        int pointSize = 10;
        Font textFont = new Font("Tahoma", 15);
        
        private void DrawBestPaths(Graphics graphicsMainPanel)
        {
            var pathPen = new Pen(Color.Red, 8);
            
            var startPoint = Constants.StartPoint.AsLocationOnPicture;
            string name = "START";

            Brush fontBrush = Brushes.Orange;

            DrawPointWithSignature(graphicsMainPanel, startPoint, name, fontBrush);

            if (personCoordinates != null)
            {
                var currentPosition = personCoordinates.AsLocationOnPicture;
                graphicsMainPanel.FillEllipse(droneIconBrush, currentPosition.X - pointSize / 2, currentPosition.Y - pointSize / 2,
                        pointSize, pointSize);
                graphicsMainPanel.DrawLine(pathPen, currentPosition, startPoint);
                graphicsMainPanel.DrawString("CZŁOWIEK", textFont, fontBrush, currentPosition.X, currentPosition.Y);

                startPoint = currentPosition;
            }
            if (objectCoordinates != null)
            {
                var currentPosition = objectCoordinates.AsLocationOnPicture;
                graphicsMainPanel.FillEllipse(droneIconBrush, currentPosition.X - pointSize / 2, currentPosition.Y - pointSize / 2,
                        pointSize, pointSize);
                graphicsMainPanel.DrawLine(pathPen, currentPosition, startPoint);
                graphicsMainPanel.DrawString("OBIEKT", textFont, fontBrush, currentPosition.X, currentPosition.Y);
                startPoint = currentPosition;
            }

        }

        private void DrawPointWithSignature(Graphics graphicsMainPanel, Point startPoint, string name, Brush fontBrush)
        {
            graphicsMainPanel.FillEllipse(droneIconBrush, startPoint.X - pointSize/2, startPoint.Y - pointSize/2,
                pointSize, pointSize);
            graphicsMainPanel.DrawString(name, textFont, fontBrush, startPoint.X, startPoint.Y);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            DrawSecondPanel();
        }


        private void saveMap_Click(object sender, EventArgs e)
        {
            int width = mapPanel.Size.Width;
            int height = mapPanel.Size.Height;


            Bitmap bm = new Bitmap(width, height);
            mapPanel.DrawToBitmap(bm, new Rectangle(0, 0, width, height));

            var graphics = Graphics.FromImage(bm);
            DrawBestPaths(graphics);
            bm.Save("mapResult.png");


            using (StreamWriter sr = new StreamWriter("wynik.txt"))
            {
                string result = "";
                if(personCoordinates!=null)
                    result += $"Poszkodowany znajduje się w punkcie: {personCoordinates?.GetUtmString()}\n";
                if(objectCoordinates!=null)
                    result += $"Przedmiot należący do poszkodowanego znajduje się w punkcie: {objectCoordinates?.GetUtmString()}\n";

                if (personCoordinates != null)
                {
                    var distance = GetDistanceFromStartTo(personCoordinates);
                    var northDistance = personCoordinates.utm.northing - Constants.StartPoint.utm.northing;
                    var eastDistance = personCoordinates.utm.easting - Constants.StartPoint.utm.easting;
                    result += $"Aby dostać się do poszkodowanego należy z punktu startu udać się" +
                              $" {northDistance:0}m na północ oraz " +
                              (eastDistance>0 ? $"{eastDistance:0}m na wschód":$"{-eastDistance:0}m na zachód") +",\n";

                    var azymut = (360.0*Math.Atan(eastDistance/northDistance)/(2*Math.PI));

                    if (azymut < 0)
                        azymut = 360.0 + azymut;

                    result += $"lub w kierunku  północno-" +
                              (eastDistance > 0 ? "wschodnim" : "zachodnim") +$", około {distance:0}m, "+
                              $" azymut {azymut:0}\n";
                    if (objectCoordinates != null)
                    {
                        var distanceOb = DistanceBetweenCoordinates(personCoordinates, objectCoordinates);
                        var northDistanceOb = objectCoordinates.utm.northing - personCoordinates.utm.northing;
                        var eastDistanceOb = objectCoordinates.utm.easting - personCoordinates.utm.easting;
                        result += $"Aby dostać się do rzeczy należącej do poszkodowanego punktu gdzie znaleziono poszkodowanego należy udać się " +
                                  (northDistanceOb > 0 ? $"{northDistanceOb:0}m na północ " : $"{-northDistanceOb:0}m na południe ")+
                                  (eastDistanceOb > 0 ? $"{eastDistanceOb:0}m na wschód" : $"{-eastDistanceOb:0}m na zachód") +",\n";


                        var azymutOb = (360.0 * Math.Atan(eastDistanceOb / northDistanceOb) / (2 * Math.PI));
                        if (azymutOb < 0)
                            azymutOb = 360 + azymutOb;

                        result += $"lub w kierunku  północno-" +
                                   (northDistanceOb > 0 ? "północno-" : "południowo-") +
                                  (eastDistanceOb > 0 ? "wschodnim" : "zachodnim") +
                                   $", około {distanceOb:0}m, " +
                                    $" azymut {azymutOb:0}";
                    }
                }

                sr.Write(result);
            }



            MessageBox.Show("Zapisano");
        }

        private void foundPointsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var p = foundPointsListBox.SelectedItem as PointInListObject;
            if (p != null)
            {
                var position = p.GpsCoordinates;
                selectedPosition = position;
                PointInfo.Text = $"Lat:  {position.latitude:0.000000}N\n" +
                                 $"Lon: {position.longitude:0.000000}E\n" +
                                 $"Alt:   {position.altitude:0.00}m \n" +
                                 $"Dist: {GetDistanceFromStartTo(position):0.00}m\n" +
                                 $"UTM: Area {position.utm.zone_number}{position.utm.zone_letter}\n" +
                                 $"N:{position.utm.northing:0.0}, E:{position.utm.easting:0.0}";
            }
            else
            {
                PointInfo.Text = "";
            }
        }

        private void SavePerson_Click(object sender, EventArgs e)
        {
            var pointInListObject = foundPointsListBox.SelectedItem as PointInListObject;
            if (pointInListObject != null)
            {
                personCoordinates = pointInListObject.GpsCoordinates;
            }
        }

        private void SaveObject_Click(object sender, EventArgs e)
        {
            var pointInListObject = foundPointsListBox.SelectedItem as PointInListObject;
            if (pointInListObject != null)
            {
                objectCoordinates = pointInListObject.GpsCoordinates;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var pointInListObject = foundPointsListBox.SelectedItem as PointInListObject;
            if (pointInListObject != null)
            {
                foundPoints.Remove(pointInListObject);
            }
        }
    }
}