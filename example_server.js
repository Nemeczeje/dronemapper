var http = require('http');

//Lets define a port we want to listen to
const PORT=8001;

const json = {
  "altitude": 99.9, 
  "latitude": 50.092528, 
  "longitude": 20.19867, 
  "utm": {
    "easting": 442682.84761308465, 
    "northing": 5549226.55459938, 
    "zone_letter": "U", 
    "zone_number": 34
  }
}

var change = 0.00003;
var changeUtm =2;
//We need a function which handles requests and send response
var counter =0;
var signLat=[-1,1,-1,1];
var signLon=[-1,-1,1,1];
var i =0;
function handleRequest(request, response){
    json.latitude += signLat[i]*change;
    json.longitude += signLon[i]*change;
    json.utm.easting += signLon[i]*changeUtm;
    json.utm.northing += signLat[i]*changeUtm;

    counter++;
    if(counter>100)
    {
      i = (i+1)%4;
      counter=0;
    }
    response.end(JSON.stringify(json));
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});