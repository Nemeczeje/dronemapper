git filter-branch --commit-filter '
        if [ "$GIT_COMMITTER_NAME" = "Marek Sosnicki" ];
        then
                GIT_COMMITTER_NAME="Marek Sosnicki";
                GIT_AUTHOR_NAME="Marek Sosnicki";
                GIT_COMMITTER_EMAIL="209191@student.pwr.edu.pl";
                GIT_AUTHOR_EMAIL="209191@student.pwr.edu.pl";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD